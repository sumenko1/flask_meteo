from dotenv import load_dotenv
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import os

load_dotenv('.env')

app = Flask(__name__)
host = os.environ.get('DB_HOST', 'localhost')
# https://www.askpython.com/python-modules/flask/flask-postgresql
app.config['SQLALCHEMY_DATABASE_URI'] = (f'postgresql://postgres:postgres@'
                                         f'{host}:5432/meteo')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)
# https://www.postgresqltutorial.com/postgresql-administration/postgresql-show-databases/


class DataStorage(db.Model):
    __tablename__ = 'meteo_datastorage'
    id = db.Column(db.Integer, primary_key=True)
    data_type_id = db.Column(db.Integer)
    time_stamp = db.Column(db.DateTime)
    value = db.Column(db.Text)

    def __repr__(self):
        time = self.time_stamp.strftime('%Y-%m-%d %H:%M:%S')
        return f'#{time} {self.value} {self.data_type_id}'

# https://programmersought.com/article/58739533755/ LIMIT ORDER etc
# sudo -u postgres -i
# postgres
# \l - list of databases
# \c meteo - switch to meteo database
# \dt - list of tables
# https://habr.com/ru/company/postgrespro/blog/326096/? - indexes

@app.route('/')
@app.route('/type/<int:data_type>')
def index(data_type=1):
    try:
        page = int(request.args.get('page'))
    except TypeError:
        page = 1

    data = DataStorage.query.filter_by(
            data_type_id=data_type).order_by(
            DataStorage.time_stamp.desc()).paginate(
            page, 10, False)
    return render_template('index.html', data=data)


@app.route('/about')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    debug = False
    if os.getenv('FLASK_DEBUG', 'False') == 'True':
        debug = True
    port = int(os.environ.get('FLASK_PORT', '5000'))
    app.run(debug=debug,
            host='0.0.0.0',
            port=port)
