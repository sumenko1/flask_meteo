FROM python:3.7-bullseye
# RUN apt update && apt -y dist-upgrade
#RUN apt install -y netcat
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /back
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
CMD python app.py
#ENTRYPOINT [ "./entrypoint.sh" ]
